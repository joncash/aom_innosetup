USE AOM_SDMS
--判斷table1中是否存在name欄位
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('MacroPlan') and name='CameraModel')
BEGIN
	ALTER TABLE MacroPlan ADD [CameraModel] [xml] NULL
END
