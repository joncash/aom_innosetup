; AOM DXF Importer 程式包裝檔案
; For Taiwan Hanbo
; ======================================================================

; 變數定義
; ======================================================================
; 目前版本 ----------------------------------------------改這裡
#define MyAppVersion "1.0.0.4"

; 廠商版本 ----------------------------------------------改這裡
#define oldVersion "1.0.0.0"

; 來源檔案路徑, 注意路徑是 Release or Debug -------------改這裡
;#define ReleaseDir "D:\00_HanboRepo\AOM_DXF_Importer\App.AOMDXFImporter\bin\Release"
#define ReleaseDir "D:\00_HanboRepo\AOM_DXF_Importer\App.AOMDXFImporter\bin\Debug"

; Exclude Files ;是否忽略 .config 檔案, -----------------改這裡
; *** Installer Setting Below
#define ExcludeFiles "App.config,*.pdb,*.log,*.vshost.*,*.application,*.manifest"

; *** Update Setting Below
;#define ExcludeFiles "App.config,*.pdb,*.log,*.vshost.*,*.application,*.manifest"


;  ***************** 設定第一次後，基本上就不再變動 ********************
; 程式名稱
#define MyAppName "AOM DXF Importer"

; 發行者
#define MyAppPublisher "Taiwan Hanbo International Technology Ltd"

; 版權宣告檔案
#define LicenseFile "D:\AOM_Installer\license.rtf"

; 執行檔名稱
#define MyAppExeName "App.AOMDXFImporter.exe"

; Program Icon File
#define MyAppIcon "D:\AOM_Installer\AOM_DXF_Importer_Data\App.ico"

; 安裝檔的圖示
#define SetupIconFile "D:\AOM_Installer\AOM_DXF_Importer_Data\AOM_DXF_Setup.ico"

; 第一次安裝必要檔案
; PowerPack
#define EnviromentDirFiles "D:\AOM_Installer\powerPack"

; 程式在開始程式集中的圖示
; 這裡的路徑是設定程式目錄中的 Data 目錄下
#define MyIco "Data\App.ico"

; 安裝檔輸出目錄 
#define OutDir "D:\Publish"

; 輸出檔名的前置詞
#define OutFile "AOM_DXF_Importer_Installer_"

;  ************* Git *******************************
#define gitPath "C:\Program Files (x86)\Git\bin"
#define gitIgnore "D:\AOM_Installer\.gitignore"
#define backupBatch "D:\backup_Hanbo.bat"
#define restoreBatch "D:\restore_Hanbo.bat"
#define workingDrive "D:"
#define restoreLog "D:\AOM_Installer\restore.txt"

;  *************************************************

;

[Setup]
;appId 是唯一值，用以識別應用程式
;這裡的 appId 是 App.AOMDXFImporter 專案的 assembly GUID
AppId={{55af7427-7423-4a03-a98b-176340269974}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}

;禁止使用者變更安裝目錄
;DisableDirPage=yes

; 指定版權宣告檔
LicenseFile={#LicenseFile}

; 輸出安裝檔的目錄位置
OutputDir={#OutDir}

; 輸出安裝檔案的完整路徑
OutputBaseFilename={#OutFile}{#MyAppVersion}

; 輸出安裝檔案的圖示
SetupIconFile={#SetupIconFile}

; 壓縮格式及參數
Compression=lzma
SolidCompression=yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

; 設定安裝選項
[Tasks]
Name: "Install";      Description: 安裝 {#MyAppVersion};  Flags: exclusive 
Name: "Restore";      Description: 還原;                  Flags: exclusive unchecked
Name: "desktopicon";  Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked


[Files]
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

; 安裝 Git ignoreFile
Source: "{#gitIgnore}";             DestDir: "{app}"; Flags: ignoreversion ;

; *********** 安裝程式檔 **********
; 指定執行檔
Source: "{#ReleaseDir}\{#MyAppExeName}"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Install ; BeforeInstall: Backup();

; 其它程式檔
Source: "{#ReleaseDir}\*"; Excludes:"{#ExcludeFiles}"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs ;Tasks: Install

; icon
Source: "{#MyAppIcon}"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Install

; Documnet
;Source: "{#ReleaseDir}\Documents\AOM使用手冊.pdf"; DestDir: "{app}"; Flags: isreadme ;Tasks: Install

; PowerPack
Source: "{#EnviromentDirFiles}\*"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Install

;還原檔案
Source: "{#restoreLog}"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Restore ; BeforeInstall: Restore();

[Icons]
; 設定程式集 shortcut
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"

; 桌面 shortcut
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

; 解除安裝 shortcut
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"

; 使用手冊在程式集中的 shortcut
Name: "{group}\{#MyAppName} 使用手冊"; Filename: "{app}\Documents\AOM DXF Importer User Manual.pdf"; WorkingDir: "{app}\Documents"

; 程式安裝完畢後是否執行
[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent


; ***** 使用者自訂程式碼 for Git ******* ;
[Code]
procedure Backup();
var 
  OutFile:  String;
  ResultCode: Integer;
begin
  OutFile:= ExpandConstant('{#backupBatch}');
  //delete first
  DeleteFile(OutFile);

  //build batch file
  SaveStringToFile(OutFile,'REM *** define variables ***' + #13#10, True);
  SaveStringToFile(OutFile,'SET gitPath={#gitPath}' + #13#10, True);
 
  SaveStringToFile(OutFile,'SET version=' + ExpandConstant('{#oldVersion}') + #13#10, True);
  SaveStringToFile(OutFile,'SET workingDrive=' + ExpandConstant('{#workingDrive}') + #13#10, True);
  SaveStringToFile(OutFile,'SET appDir=' + ExpandConstant('{app}') + #13#10, True);
  SaveStringToFile(OutFile,'SET backupTime=%date:~0,10% %time%' + #13#10, True);  
  SaveStringToFile(OutFile,'SET backupMsg="backup, %backupTime%"' + #13#10, True);  
  
  SaveStringToFile(OutFile,'SET tagMsg="backup version %version%"' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** setting the git.exe into system path ***' + #13#10, True);
  SaveStringToFile(OutFile,'PATH %gitPath%;%PATH%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** change the directory to AOM DXF directory ***' + #13#10, True);
  SaveStringToFile(OutFile,'%workingDrive%' + #13#10, True);
  SaveStringToFile(OutFile,'cd %appDir%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** add and commit current version ***' + #13#10, True);
  SaveStringToFile(OutFile,'git add --all' + #13#10, True);
  SaveStringToFile(OutFile,'git commit -m %backupMsg%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** Tag it, in order to restore files easily ***' + #13#10, True);
  SaveStringToFile(OutFile,'git tag -a %version% -m %tagMsg%' + #13#10, True);

  //Execute batch file
 if Exec(OutFile, '', '', SW_SHOW,
     ewWaitUntilTerminated, ResultCode) then
  begin
    log('Success');
    // handle success if necessary; ResultCode contains the exit code
  end 
  else begin
    log('Fail');
    // handle failure if necessary; ResultCode contains the error code
  end;

end;
procedure Restore();
var 
  OutFile:  String;
  ResultCode: Integer;
begin
  OutFile:=ExpandConstant('{#restoreBatch}');
    //delete first
  DeleteFile(OutFile);

  //build batch file
  SaveStringToFile(OutFile,'REM *** Restore Application ***' + #13#10, True);
  SaveStringToFile(OutFile,'REM *** define variables ***' + #13#10, True);
  SaveStringToFile(OutFile,'SET gitPath={#gitPath}' + #13#10, True);
 
  SaveStringToFile(OutFile,'SET version=' + ExpandConstant('{#oldVersion}') + #13#10, True);
  SaveStringToFile(OutFile,'SET workingDrive=' + ExpandConstant('{#workingDrive}') + #13#10, True);
  SaveStringToFile(OutFile,'SET appDir=' + ExpandConstant('{app}') + #13#10, True);
  SaveStringToFile(OutFile,'SET backupTime=%date:~0,10% %time%' + #13#10, True);  
  SaveStringToFile(OutFile,'SET backupMsg="restore, %backupTime%"' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** setting the git.exe into system path ***' + #13#10, True);
  SaveStringToFile(OutFile,'PATH %gitPath%;%PATH%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** change the directory to AOM DXF directory ***' + #13#10, True);
  SaveStringToFile(OutFile,'%workingDrive%' + #13#10, True);
  SaveStringToFile(OutFile,'cd %appDir%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** add and commit current version ***' + #13#10, True);
  SaveStringToFile(OutFile,'git add --all' + #13#10, True);
  SaveStringToFile(OutFile,'git commit -m %backupMsg%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** restore files ***' + #13#10, True);
  SaveStringToFile(OutFile,'git reset --hard %version%' + #13#10, True);
  
  //Execute batchFile
  if Exec(OutFile, '', '', SW_SHOW,
     ewWaitUntilTerminated, ResultCode) then
  begin
    log('Success');
    // handle success if necessary; ResultCode contains the exit code
  end 
  else begin
    log('Fail');
    // handle failure if necessary; ResultCode contains the error code
  end;
end;