; AOM Release Setup 
; ========================  variables define ==============================

#define MyAppName "AOM ALL Of Measureing - H View"
;目前版本---------------------------------------改這裡
#define MyAppVersion "1.1.0.279"

;廠商版本 1.1.0.37---------------------------------------改這裡
#define oldVersion "1.1.0.277"

;輸出目錄及檔名---------------------------------------改這裡
#define OutDir "D:\Publish"
#define OutFile "AOM_HView_Develop_Patch_"


#define MyAppPublisher "Taiwan Hanbo International Technology Ltd"
#define MyAppExeName "App.SDMS_HView.exe"



; ************* 來源檔案路徑, 注意是 Release or Debug---------------------------------------改這裡
;#define ReleaseDir "D:\01_Repos\Repo\aom-winapp\App.SDMS\bin\Release"
#define ReleaseDir "D:\00_HanboRepo\aom\App.SDMS\bin\Debug_HView"

;裝置設定執行檔
#define AOMConfiguratorExe "D:\AOM_Installer\AOMAdditionalExe\AOMConfigurator.exe"
#define AOMConfiguratorName "AOMConfigurator.exe"
#define AOMConfiguratorICO "D:\AOM_Installer\AOMAdditionalExe\AOMConfigurator.ico"
#define AOMConfiguratorICOName "AOMConfigurator.ico"

;版權宣告檔案
#define LicenseFile "D:\AOM_Installer\license.rtf"


;Setup Icon File
#define SetupIconFile "D:\AOM_Installer\Settings.ico"

;Program Icon File
#define MyAppIcon "D:\AOM_Installer\Hanbo_Horizontal.ico"

;程式的圖示
#define MyIco "Configuration\Images\Hanbo_Horizontal.ico"

;第一次安裝必要檔案
#define EnviromentDirFiles "D:\AOM_Installer\powerPack"

;************ Exclude Files ;是否忽略 .config 檔案, 要注意==================== 改這裡
;*** Installer Setting Below
;#define ExcludeFiles "App.config,*.pdb,*.tiff,*.shm,*.log,*.vshost.*,*.application,*.manifest"

;*** Update Setting Below
#define ExcludeFiles "App.config,*.pdb,*.tiff,*.shm,*.log,*.vshost.*,*.application,*.manifest,*.config,Mahr.xml,CameraSetting.*,AutoExportSetting.csv,CameraSpecifications.xml,LightDefaultSettings.xml,\Configuration\Settings.xml,ExportSetting.xml,CustomFunctions.xml,CameraSettings.xml,UserProfile.xml,\Exe\OptoMagnify\*.*,\Configuration\Images\*.*,\data\*.*,\Icon\*.*,\Documents\*.*"

;========== Git ==================
#define gitPath "C:\Program Files (x86)\Git\bin"
#define gitIgnore "D:\AOM_Installer\.gitignore"
#define backupBatch "D:\backup_Hanbo.bat"
#define restoreBatch "D:\restore_Hanbo.bat"
#define workingDrive "D:"
#define restoreLog "D:\AOM_Installer\restore.txt"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{A5F60203-DC4C-4368-B2C6-142166C07D4B}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
DefaultDirName={pf}\HanboAOM
DefaultGroupName=HanboAOM
DisableProgramGroupPage=yes
LicenseFile={#LicenseFile}
OutputDir={#OutDir}
OutputBaseFilename={#OutFile}{#MyAppVersion}
SetupIconFile={#SetupIconFile}
Compression=lzma
SolidCompression=yes
SetupLogging=yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "Install";      Description: 安裝 {#MyAppVersion};  Flags: exclusive 
Name: "Restore";      Description: 還原;                  Flags: exclusive unchecked
Name: "desktopicon";  Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
;Git ignoreFile
Source: "{#gitIgnore}";             DestDir: "{app}"; Flags: ignoreversion ;
;安裝檔案
Source: "{#ReleaseDir}\{#MyAppExeName}"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Install ; BeforeInstall: Backup();
Source: "{#ReleaseDir}\*"; Excludes:"{#ExcludeFiles}"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs ;Tasks: Install
Source: "{#ReleaseDir}\Configuration\Default\*"; DestDir: "{app}\Configuration\Default"; Flags: ignoreversion recursesubdirs createallsubdirs ;Tasks: Install
Source: "{#MyAppIcon}"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Install
Source: "{#ReleaseDir}\App.ThreeColorLightBuzzerConfigurator.exe.config"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Install
;Source: "{#AOMConfiguratorExe}"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Install
;Source: "{#AOMConfiguratorICO}"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Install
;Source: "{#ReleaseDir}\Documents\AOM使用手冊.pdf"; DestDir: "{app}"; Flags: isreadme ;Tasks: Install
;Source: "{#EnviromentDirFiles}\*"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Install


;還原檔案
Source: "{#restoreLog}"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Restore ; BeforeInstall: Restore();
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; IconFilename: "{app}\{#MyIco}";
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{userdesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}";WorkingDir: "{app}"; IconFilename: "{app}\{#MyIco}"; Comment: "Shortcut"
;Document
Name: "{group}\{#MyAppName} 使用手冊"; Filename: "{app}\Documents\AOM使用手冊.pdf"; WorkingDir: "{app}\Documents"
Name: "{group}\三色蜂鳴燈設定檔使用說明書.pdf"; Filename: "{app}\Documents\三色蜂鳴燈 Configurator 使用說明書.pdf"; WorkingDir: "{app}\Documents"
;AOMConfigurator
Name: "{group}\{#AOMConfiguratorName}"; Filename: "{app}\{#AOMConfiguratorName}"; IconFilename: "{app}\{#AOMConfiguratorICOName}"
;三色燈設定
Name: "{group}\App.ThreeColorLightBuzzerConfigurator.exe"; Filename: "{app}\App.ThreeColorLightBuzzerConfigurator.exe";



[Code]
procedure Backup();
var 
  OutFile:  String;
  ResultCode: Integer;
begin
  OutFile:= ExpandConstant('{#backupBatch}');
  //delete first
  DeleteFile(OutFile);

  //build batch file
  SaveStringToFile(OutFile,'REM *** define variables ***' + #13#10, True);
  SaveStringToFile(OutFile,'SET gitPath={#gitPath}' + #13#10, True);
 
  SaveStringToFile(OutFile,'SET version=' + ExpandConstant('{#oldVersion}') + #13#10, True);
  SaveStringToFile(OutFile,'SET workingDrive=' + ExpandConstant('{#workingDrive}') + #13#10, True);
  SaveStringToFile(OutFile,'SET appDir=' + ExpandConstant('{app}') + #13#10, True);
  SaveStringToFile(OutFile,'SET backupTime=%date:~0,10% %time%' + #13#10, True);  
  SaveStringToFile(OutFile,'SET backupMsg="backup, %backupTime%"' + #13#10, True);  
  
  SaveStringToFile(OutFile,'SET tagMsg="backup version %version%"' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** setting the git.exe into system path ***' + #13#10, True);
  SaveStringToFile(OutFile,'PATH %gitPath%;%PATH%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** change the directory to AOM directory ***' + #13#10, True);
  SaveStringToFile(OutFile,'%workingDrive%' + #13#10, True);
  SaveStringToFile(OutFile,'cd %appDir%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** add and commit current version ***' + #13#10, True);
  SaveStringToFile(OutFile,'git add --all' + #13#10, True);
  SaveStringToFile(OutFile,'git commit -m %backupMsg%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** Tag it, in order to restore files easily ***' + #13#10, True);
  SaveStringToFile(OutFile,'git tag -a %version% -m %tagMsg%' + #13#10, True);

  //Execute batch file
 if Exec(OutFile, '', '', SW_SHOW,
     ewWaitUntilTerminated, ResultCode) then
  begin
    log('Success');
    // handle success if necessary; ResultCode contains the exit code
  end 
  else begin
    log('Fail');
    // handle failure if necessary; ResultCode contains the error code
  end;

end;
procedure Restore();
var 
  OutFile:  String;
  ResultCode: Integer;
begin
  OutFile:=ExpandConstant('{#restoreBatch}');
    //delete first
  DeleteFile(OutFile);

  //build batch file
  SaveStringToFile(OutFile,'REM *** Restore Application ***' + #13#10, True);
  SaveStringToFile(OutFile,'REM *** define variables ***' + #13#10, True);
  SaveStringToFile(OutFile,'SET gitPath={#gitPath}' + #13#10, True);
 
  SaveStringToFile(OutFile,'SET version=' + ExpandConstant('{#oldVersion}') + #13#10, True);
  SaveStringToFile(OutFile,'SET workingDrive=' + ExpandConstant('{#workingDrive}') + #13#10, True);
  SaveStringToFile(OutFile,'SET appDir=' + ExpandConstant('{app}') + #13#10, True);
  SaveStringToFile(OutFile,'SET backupTime=%date:~0,10% %time%' + #13#10, True);  
  SaveStringToFile(OutFile,'SET backupMsg="restore, %backupTime%"' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** setting the git.exe into system path ***' + #13#10, True);
  SaveStringToFile(OutFile,'PATH %gitPath%;%PATH%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** change the directory to AOM directory ***' + #13#10, True);
  SaveStringToFile(OutFile,'%workingDrive%' + #13#10, True);
  SaveStringToFile(OutFile,'cd %appDir%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** add and commit current version ***' + #13#10, True);
  SaveStringToFile(OutFile,'git add --all' + #13#10, True);
  SaveStringToFile(OutFile,'git commit -m %backupMsg%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** restore files ***' + #13#10, True);
  SaveStringToFile(OutFile,'git reset --hard %version%' + #13#10, True);
  
  //Execute batchFile
  if Exec(OutFile, '', '', SW_SHOW,
     ewWaitUntilTerminated, ResultCode) then
  begin
    log('Success');
    // handle success if necessary; ResultCode contains the exit code
  end 
  else begin
    log('Fail');
    // handle failure if necessary; ResultCode contains the error code
  end;
end;
